///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file util.c
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "main.h"

int moveCatBackOneIndex(struct cat catStructArray[], int index) {
    *(catStructArray + index) = *(catStructArray + index + 1);
    return 0;
}

bool isDbFull(size_t totalCats) {
    return totalCats == MAX_CATS ? true:false;
}

bool isCatNameEmpty(char name[]) {
    return strlen(name) > 0 ? false:true;
}

bool isNameLargerThan30Char(char name[]) {
    return strlen(name) > 30 ? true:false;
}

bool isWeightLargerThanZero(float* weight) {
    return *weight > 0.0 ? true:false;
}