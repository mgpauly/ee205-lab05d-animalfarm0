///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "main.h"

int addCat(struct cat catStructArray[], size_t* totalCats, char name[], catGender gender, catBreed breed, bool isFixed, float* weight) {

#ifdef DEBUG
        printf("db = %s\n", isDbFull(*totalCats)?"true":"false");
        printf("isCatNameEmpty = %s\n", isCatNameEmpty(name)?"true":"false");
        printf("isNameLargerThan30Char = %s\n", isNameLargerThan30Char(name)?"true":"false");
        printf("weight = %f\n", weight);
        printf("isWeightLargerThanZero = %s\n", isWeightLargerThanZero(&weight)?"true":"false");
#endif
        strcpy(catStructArray[*totalCats].name, name);
        catStructArray[*totalCats].gender = gender;
        catStructArray[*totalCats].breed = breed;
        catStructArray[*totalCats].isFixed = isFixed;
        catStructArray[*totalCats].weight = *weight;
        *totalCats = *totalCats + 1;
    
    return 0;
}