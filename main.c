///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "main.h"

int main() {
    printf("Starting Animal Farm 0\n\n");

    struct cat catArray[MAX_CATS];
    size_t totalCats = 0;
    initializeCatsStructArray(catArray);

    float w = 8.5, w2 = 7.0, w3 = 18.2, w4 = 9.2, w5 = 12.2, w6 = 19.0;
    addCat(catArray, &totalCats, "Loki", MALE, PERSIAN, true, &w);
    addCat(catArray, &totalCats, "Milo", MALE, MANX, true, &w2);
    addCat(catArray, &totalCats, "Bella", FEMALE, MAINE_COON, true,  &w3);
    addCat(catArray, &totalCats, "Kali", FEMALE, SHORTHAIR, false, &w4);
    addCat(catArray, &totalCats, "Trin", FEMALE, MANX, true,  &w5);
    addCat(catArray, &totalCats, "Chili", UNKNOWN_GENDER, SHORTHAIR, false,  &w6);
    printAllCats(catArray, &totalCats);
    printf("\n");

    int kali = findCat(catArray, &totalCats, "Kali");
    updateCatName(catArray, &totalCats, kali, "Chili");
    printCat(catArray, &totalCats, kali);
    updateCatName(catArray, &totalCats, kali, "Capulet");
    fixCat(catArray, kali);
    printCat(catArray, &totalCats, kali);
    printf("\n");

    printAllCats(catArray, &totalCats);
    printf("\n");

    deleteCat(catArray, &totalCats, kali);
    printAllCats(catArray, &totalCats);
    printf("\n");

    deleteAllCats(catArray, &totalCats);
    printAllCats(catArray, &totalCats);
    printf("\n");

    printf("Done with Animal Farm 0\n");
    return 0;
}