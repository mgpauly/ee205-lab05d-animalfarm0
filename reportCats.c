///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "main.h"

int printCat(struct cat catStructArray[], size_t* totalCats, int index) {
    // less than 0
    if (index < 0) {
        printf("Bad Code [%d]\n", index);
        return 0;
    }

    //more than number of cats in database
    if (index >= totalCats) {
        printf("Bad Cat [%d]\n", index);
        return 0;
    }

    struct cat currentCat = catStructArray[index];
    printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", index, currentCat.name, currentCat.gender, currentCat.breed, currentCat.isFixed, currentCat.weight);
    return 0;
}

int printAllCats(struct cat catStructArray[], size_t* totalCats) {

    if (*totalCats == 0) {
        printf("There are no cats on the farm.\n");
        return 0;
    }

    for (int i = 0; i < *totalCats; i++) {
        printCat(catStructArray, (unsigned long)*totalCats, i);
    }
    return 0;
}

int findCat(struct cat catStructArray[], size_t* totalCats, char name[MAX_CAT_NAME]) {
    for (int i = 0; i < *totalCats; i++) {
        if (!strcmp(catStructArray[i].name, name)) {
            return i;
        }
    }
    return -1;
}
