animalFarm:
	gcc -o animalFarm0 addCats.c catDatabase.c deleteCats.c main.c reportCats.c updateCats.c test.c util.c

test:
	gcc -o test addCats.c catDatabase.c deleteCats.c main.c reportCats.c updateCats.c test.c util.c

clean:
	rm test
