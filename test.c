///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file test.c
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "main.h"

int catDatabaseTest() {
    struct cat cats[MAX_CATS];
    struct cat newCat = {.name="Max\0", .gender=MALE, .breed=MAINE_COON, .isFixed=true, .weight=12.2};
    struct cat newCat2 = {.name="Ashten\0", .gender=MALE, .breed=MAINE_COON, .isFixed=true, .weight=12.2};
    cats[1] = newCat2;

    printf("cat1: %s %s %s %s %f\n", cats[0].name, cats[0].gender==1?"MALE":"NotMAle", cats[0].breed==1?"Maine Coon":"something else", cats[0].isFixed?"true":"false", cats[0].weight);
    printf("cat1: %s %s %s %s %f\n", cats[1].name, cats[1].gender==1?"MALE":"NotMAle", cats[1].breed==1?"Maine Coon":"something else", cats[1].isFixed?"true":"false", cats[1].weight);

    initializeCatsStructArray(cats);

    printf("cat1: %s %s %s %s %f\n", cats[0].name, cats[0].gender==1?"MALE":"NotMAle", cats[0].breed==1?"Maine Coon":"something else", cats[0].isFixed?"true":"false", cats[0].weight);
    printf("cat1: %s %s %s %s %f\n", cats[1].name, cats[1].gender==1?"MALE":"NotMAle", cats[1].breed==1?"Maine Coon":"something else", cats[1].isFixed?"true":"false", cats[1].weight);

    return 0;
}

int addCatTests() {

    //isDbFull
    size_t testTotalCats = MAX_CATS;
    bool isFull = isDbFull(testTotalCats);
    printf("totalCats = %lu\n", testTotalCats);
    printf(isFull?"full\n":"notFull\n");
    testTotalCats = 0;
    isFull = isDbFull(testTotalCats);
    printf("totalCats = %lu\n", testTotalCats);
    printf("isDBFull = %s\n\n",isFull?"full":"notFull");

    //isCatNameEmpty
    char name[MAX_CAT_NAME] = "\0";
    printf("name = %s\n", name);
    printf("isCatNameEmpty = %s\n", isCatNameEmpty(name)?"true":"false");
    strcpy(name, "newName");
    printf("name = %s\n", name);
    printf("isCatNameEmpty = %s\n\n", isCatNameEmpty(name)?"true":"false");

    //isNameLargerThan30Char
    printf("name = %s\n", name);
    printf("isNameLargerThan30Char = %s\n", isNameLargerThan30Char(name)?"true":"false");
    strcpy(name, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    printf("name = %s\n", name);
    printf("isNameLargerThan30Char = %s\n\n", isNameLargerThan30Char(name)?"true":"false");

    //isWeightLargerThanZero
    float weight = 0.0;
    printf("weight = %f\n", weight);
    printf("isWeightLargerThanZero = %s\n", isWeightLargerThanZero(&weight)?"true":"false");
    weight = 1.0;
    printf("weight = %f\n", weight);
    printf("isWeightLargerThanZero = %s\n\n", isWeightLargerThanZero(&weight)?"true":"false");
    
    //addCat
    struct cat testCatStructArray[MAX_CATS];
    initializeCatsStructArray(testCatStructArray);
    printf("testTotalCats = %lu\n", testTotalCats);
    printf("cat[0].name = %s\n", testCatStructArray[0].name);
    float w = 3.8;
    addCat(testCatStructArray, &testTotalCats, "mittens", MALE, MAINE_COON, true, &w);
    printf("testTotalCats = %lu\n", testTotalCats);
    printf("cat[0].name = %s cat[0].float = %f\n\n", testCatStructArray[0].name, testCatStructArray[0].weight);
    
    return 0;
}

int deleteCatsTest() {

    struct cat testCatStructArray[MAX_CATS];
    size_t testTotalCats = 0;
    float w = 8.92;
    float w2 = 3.2;
    float w3 = 3.4;
    float w4 = 3.5;
    float w5 = 89.3;
    float w6 = 3.2;
    addCat(testCatStructArray, &testTotalCats, "mittens", MALE, MAINE_COON, true, &w);
    addCat(testCatStructArray, &testTotalCats, "jess", MALE, MAINE_COON, true, &w6);
    addCat(testCatStructArray, &testTotalCats, "anothony", MALE, MAINE_COON, true, &w2);
    addCat(testCatStructArray, &testTotalCats, "tim", MALE, MAINE_COON, true, &w3);
    addCat(testCatStructArray, &testTotalCats, "john", MALE, MAINE_COON, true, &w4);
    addCat(testCatStructArray, &testTotalCats, "sam", MALE, MAINE_COON, true, &w5);
    printAllCats(testCatStructArray, &testTotalCats);
    printf("\n\n");
    deleteCat(testCatStructArray, &testTotalCats, 4);
    printAllCats(testCatStructArray, &testTotalCats);
    deleteAllCats(testCatStructArray, &testTotalCats);
    printAllCats(testCatStructArray, &testTotalCats);

    return 0;
}

int updateCatsTest() {
    struct cat testCatStructArray[MAX_CATS];
    size_t testTotalCats = 0;
    initializeCatsStructArray(testCatStructArray);
    float w = 14.32;
    addCat(testCatStructArray, &testTotalCats, "mittens", MALE, MAINE_COON, false, &w);
    float w2 = 32.1;
    addCat(testCatStructArray, &testTotalCats, "ralph", MALE, MAINE_COON, false, &w2);

    updateCatName(testCatStructArray, &testTotalCats, 0, "ralph");
    updateCatName(testCatStructArray, &testTotalCats, 0, "kevin");
    updateCatName(testCatStructArray, &testTotalCats, 0, "carter");
    updateCatName(testCatStructArray, &testTotalCats, 0, "");

    fixCat(testCatStructArray, 0);

    float w3 = 11.2;
    updateCatWeight(testCatStructArray, 0, &w3);

    return 0;
}

int reportCatsTest() {
    struct cat testCatArray[MAX_CATS];
    size_t testTotalCats = 0;
    initializeCatsStructArray(testCatArray);
    float w = 13.8;
    initializeCatsStructArray(testCatArray);
    addCat(testCatArray, &testTotalCats, "mittens", MALE, MAINE_COON, true, &w);
    float w2 = 18.2;
    addCat(testCatArray, &testTotalCats, "ginger", FEMALE, UNKNOWN_BREED, false, &w2);
    //print cat
    printCat(testCatArray, testTotalCats, 0);
    printf("\n");

    //find cat
    float w3 = 19.2;
    addCat(testCatArray, &testTotalCats, "johnson", MALE, MAINE_COON, false, &w3);
    printf("findCat = %d\n", findCat(testCatArray, &testTotalCats, "mittens"));
    printf("findCat = %d\n", findCat(testCatArray, &testTotalCats, "ginger"));
    printf("findCat = %d\n", findCat(testCatArray, &testTotalCats, "johnson"));
    printf("findCat = %d\n", findCat(testCatArray, &testTotalCats, "niice"));
    printf("findCat = %d\n\n", findCat(testCatArray, &testTotalCats, "jokjniice"));

    //print all cats
    printAllCats(testCatArray, testTotalCats);
    return 0;
}
void printStars() {
    printf("##################################################\n");
}

#ifdef DEBUG
int main() {
    printf("Data Base Test\n");
    printStars();
    printf(catDatabaseTest()?"test failed\n":"tests passed\n");
    printf("\n\n");

    printf("Add Cats Test\n");
    printStars();
    printf(addCatTests()?"tests failed\n":"tests passed\n");
    printf("\n\n");

    printf("Delete Cats Test\n");
    printStars();
    printf(deleteCatsTest()?"tests failed\n":"tests passed\n");
    printf("\n\n");

    printf("Update Cats Test\n");
    printStars();
    printf(updateCatsTest()?"tests failed\n":"tests passed\n");
    printf("\n\n");

    printf("Report Cats Test\n");
    printStars();
    printf(reportCatsTest()?"tests failed\n":"tests passed\n");
    printf("\n\n");

    printf("Tests Complete\n");

    return 0;
}
#endif