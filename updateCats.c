///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "main.h"

int updateCatName(struct cat catStructArray[], size_t* totalCats, int index, char newName[]) {

    if (findCat(catStructArray, totalCats, newName) >= 0) {
        printf("Sorry, %s is already a cat on the farm.\n", newName);
        return 1;
    }

    if (strlen(newName) <= 0) {
        printf("Sorry, cats need to have a name longer than 0 characters.\n");
        return 1;
    }


    #ifdef DEBUG
    printf("newName = %s\n", newName);
    printf("current index name = %s\n", catStructArray[index].name);
    #endif

    char oldName[MAX_CAT_NAME];
    strcpy(oldName, catStructArray[index].name);
    strcpy(catStructArray[index].name, newName);
    printf("You changed %s's name to %s\n", oldName, catStructArray[index].name);

    #ifdef DEBUG
    printf("final index name = %s\n", catStructArray[index].name);
    #endif
    return 0;
}

void fixCat(struct cat catStructArray[], int index) {
    bool oldState = catStructArray[index].isFixed;
    catStructArray[index].isFixed = !oldState;
    printf("%s is now %s\n", catStructArray[index].name, catStructArray[index].isFixed?"fixed":"not fixed");
    #ifdef DEBUG
    printf("old state = %s\n",oldState?"fixed":"notfixed");
    printf("new state = %s\n", catStructArray[index].isFixed?"fixed":"notfixed");
    #endif
}

int updateCatWeight(struct cat catStructArray[], int index, float* newWeight) {

    if (*newWeight <= 0) {
        printf("The new weight needs to be > 0\n");
        return 1;
    }

    #ifdef DEBUG
    printf("current weight = %f\n", catStructArray[index].weight);
    #endif
    catStructArray[index].weight = *newWeight;
    printf("%s's new weight is %f\n", catStructArray[index].name, catStructArray[index].weight);
    #ifdef DEBUG
    printf("final weight = %f\n", catStructArray[index].weight);
    #endif
    return 0;
}